import { config as load_dotenv } from "dotenv"


export class Env {
    constructor() {
        load_dotenv();
        this.token = process.env.TOKEN;
        this.guild = process.env.GUILD;
        this.able_to_speak = (process.env.ABLE_TO_SPEAK || "both").toLowerCase();
        this.minMessageLength = process.env.MIN_MESSAGE_LENGTH || 50;
        this.slowTime = Number(process.env.SLOW_TIME || 60) || 60;

        const threads = process.env.THREADS_CHANNELS || ''
        if (threads) {
            this.threadsChannels = threads.split(",").filter(Boolean);
        }
        else {
            this.threadsChannels = [];
            console.warn("THREADS_CHANNELS is not defined, falling back to []");
        }
        this.checkenv();
    }

    checkenv() {
        if (!this.token) {
            throw new Error("TOKEN is not defined");
        }
        if (!this.guild) {
            throw new Error("GUILD is not defined");
        }
    }
}
