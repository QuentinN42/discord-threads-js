import { Client, Intents } from "discord.js"


export class Bot {
    constructor(environment) {
        this.env = environment;
        this.client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });

        this.client.once("ready", async () => {
            console.log(`Logged in as "${this.client.user.tag}" !`);
        })

        this.registerActions();
    }

    registerActions() {
        this.client.on('messageCreate', this.processMessage.bind(this));
    }

    async processMessage(message) {
        if (! this.messageInValidThread(message)) { return }
        if (! this.isValidUser(message.author))   { return this.deleteAndNotify(message, "You are not able to speak here.");}        

        // Check content and slow mode
        if (! this.isValidMessage(message))       { return this.deleteAndNotify(message, "Too short message.");}        
        if (! await this.checkSlowMode(message))  { return this.deleteAndNotify(message, "Too fast, please wait before resending a new message.");}

        this.createThread(message);
    }

    start() {
        this.client.login(this.env.token);
    }

    messageInValidThread(message) {
        return message.guildId === this.env.guild && this.env.threadsChannels.includes(message.channelId);
    }

    createThread(message) {
        const title = message.content.split("\n")[0].substring(0,99);
        message.startThread({name: title});
    }

    isValidMessage(message) {
        return message.content.length > this.env.minMessageLength && message.content.split("\n").length > 1;
    }

    isValidUser(user) {
        if (this.env.able_to_speak === "both"              ) {return true;}
        if (this.env.able_to_speak === "bot"   &&  user.bot) {return true;}
        if (this.env.able_to_speak === "human" && !user.bot) {return true;}
        return false;
    }

    deleteAndNotify(message, error) {
        message.delete();
        if (!message.author.bot) {
            message.author.send({
                "content": "Your message has been deleted !",
                "tts": false,
                "embeds": [
                    {
                        "type": "rich",
                        "title": "Error",
                        "description": error,
                        "color": 0xff0000
                    }
                ]
            });
        }
    }

    async checkSlowMode(message) {
        const channel = message.guild.channels.cache.get(message.channelId);
        const messages = await channel.messages.fetch({ limit: 2 });
        const lastMessage = messages.last();
        const deltaT = (message.createdTimestamp - lastMessage.createdTimestamp) / 1000;
        return deltaT > this.env.slowTime;
    }
}
