import { Env } from "./config.js"
import { Bot } from "./bot.js"


const env = new Env();
const bot = new Bot(env);

bot.start();
