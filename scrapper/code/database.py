import os

from peewee import Model, CharField, TextField, BooleanField, SqliteDatabase, IntegerField, ForeignKeyField

database_path = os.path.join(os.path.dirname(__file__), "../data/database.db")

print(database_path)

db = SqliteDatabase(database_path)


class BaseModel(Model):
    class Meta:
        database = db


class Group(BaseModel):
    group_id = CharField(primary_key=True, max_length=30)
    name = TextField()


class Post(BaseModel):
    post_id = CharField(primary_key=True, max_length=30)
    group_id = ForeignKeyField(Group, backref="posts")
    timestamp = IntegerField()
    username = TextField()
    text = TextField()
    link = TextField(default=None, null=True)
    image_url = TextField(default=None, null=True)
    published = BooleanField(default=False, null=True)


def init_db():
    db.connect()
    db.create_tables([Group, Post])


def reset_db():
    db.drop_tables([Group, Post])
    db.create_tables([Group, Post])


def close_db():
    db.close()
