from discord import Webhook, AsyncWebhookAdapter
import yaml
import os
import logging
import aiohttp

from discord.ext import commands

datapath = lambda a: os.path.join(os.path.dirname(__file__), f"../data/{a}")

config = None

with open(datapath("config.yml")) as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

logging.basicConfig(level=config['loglevel'])

bot = commands.Bot(command_prefix=config['discord']['cmd_prefix'])


async def send_as(msg: str, author: str):
    async with aiohttp.ClientSession() as session:
        webhook = Webhook.from_url(config['discord']['webhook_url'], adapter=AsyncWebhookAdapter(session))
        await webhook.send(msg, username=author)


@bot.command()
async def ping(ctx):
    await ctx.send("pong !")


@bot.command()
async def bind(ctx):
    print(ctx)


@bot.command()
async def say(ctx, arg1, arg2):
    await send_as(arg2, arg1)


try:
    bot.run(config['discord']['token'])
except KeyboardInterrupt:
    print("salut")
    bot.close()
