import json
from facebook_scraper import get_posts
import os
import datetime
import yaml

from database import Post, Group, init_db, close_db, reset_db

config = None

datapath = lambda a: os.path.join(os.path.dirname(__file__), f"../data/{a}")

with open(datapath("config.yml")) as f:
    config = yaml.load(f, Loader=yaml.FullLoader)


def log(message, level=3):
    now = datetime.datetime.now()
    if config['loglevel'] == 'DEBUG':
        current_time = now.strftime("%H:%M:%S")
        print(f"{current_time} - {message}")
    else:
        current_date = now.strftime("%H:%M:%S")
        with open(datapath(config['logfile']), "a") as file:
            file.write(f"{message}\n")


def add_group(id, name):
    group = Group(group_id=str(id), name=name)


def scrape(group: Group, pages: int):
    posts = get_posts(group=group.group_id, pages=pages, cookies=datapath("cookies.json"),
                      options={"allow_extra_requests": True})
    save_posts(posts)


def save_posts(posts):
    results = []
    for entry in posts:
        post, created = Post.get_or_create(
            post_id=str(entry['post_id']), defaults={
                "group_id": str(entry['page_id']),
                "timestamp": int(entry['timestamp']),
                "username": entry['username'],
                "text": entry['text'],
                "link": entry['link'],
                "image_url": (entry['image_lowquality'] if entry['image_id'] else None),
            }
        )
        if created:
            log(f"Added post {post.post_id}")
        post.save()


def init_groups():
    for group in config['groups']:
        add_group(group['id'], group['name'])


def main():
    init_db()
    reset_db()
    init_groups()
    with open(datapath("results.json")) as file:
        posts = json.load(file)
        save_posts(posts)


if __name__ == "__main__":
    main()
